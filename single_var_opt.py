"""
Created on Tue Sep 27 18:51:32 2016
Completed on    

@author: arun
"""

# Bisection Method as given in CS Theory.lyx
# Created on Tue Sep 27 18:51:32 2016
# Completed on Sat Oct 1 18:51:32 2016

def bisection(f, a, b, tol):
    # set tolerance if not provided by user
    if tol == 0:
        tol = 0.0005
    x_a = a
    x_b = b
    x_m = (x_a + x_b) * 0.5
    f_a = f(x_a)
    f_b = f(x_b)
    f_m = f(x_m)       

    while abs(x_b - x_a) >= tol:	
        #print (f_a,f_b,f_m,x_m)
        x_l = (x_a + x_m) * 0.5
        x_r = (x_m + x_b) * 0.5
        f_l = f(x_l)
        f_r = f(x_r)
        f_min = min(f_a, f_b, f_m, f_l, f_r)	
        if f_min == f_a or f_min == f_l:
            x_b = x_m
            x_m = x_l
            f_b = f_m
            f_m = f_l
        elif f_min == f_m:
            x_a = x_l
            x_b = x_r
            f_a = f_l
            f_b = f_r
        elif f_min == f_r or f_min == f_b:
            x_a = x_m
            x_m = x_r
            f_a = f_m
            f_m = f_r
    return x_m




# Problem: minimize F (x) = x^3 - 3 x^2
# for 0 <= x <= 3

# REVISE TO MAKE IT MORE GENERAL
# REVISE THE BRUTE-FORCE CODE!

# Golden Section Method for question given in Example 2.2, LUT Optimization
# Change this to reflect new problems

import math
phi = (math.sqrt(5) + 1) / 2 # golden ratio

def f(x):
    return abs(x-0.7)

def golden_section_search(x_a, x_b, tolerance):    
    x_c = x_b - (x_b - x_a) / phi
    x_d = x_a + (x_b - x_a) / phi    
    
    while abs(x_b-x_a) >= tolerance:
        print (x_a, x_b, x_b - x_a, x_c, x_d, f(x_c),f(x_d))
        if f(x_c) < f(x_d):
            x_b = x_d
        else:
            x_a = x_c
        x_c = x_b - (x_b - x_a) / phi
        x_d = x_a + (x_b - x_a) / phi                    
    print (x_a,x_b)
    return (x_a + x_b)/2

tolerance = 0.25 # tolerance
x_a = 0
x_b = 2
    	
print(golden_section_search(x_a, x_b, tolerance))


