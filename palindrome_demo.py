# August 20, 2014

def palindrome_recur(string):
    new_string = string.replace(" ", "")
    #print (new_string)
    #print(new_string[0], new_string[-1], new_string[0]==new_string[-1])
    if len(new_string) <= 1:
        return True
    return new_string[0]==new_string[-1] and palindrome_recur(new_string[1:-1])
print(palindrome_recur("was it a car or a cat i saw"))
