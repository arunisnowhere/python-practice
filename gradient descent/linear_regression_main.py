# -*- coding: utf-8 -*-
"""
Created on Sat Dec 24 14:23:52 2016

@author: arun
"""

import numpy as np
import linear_regression

print('Loading data ...\n');

# Load Data
data = np.loadtxt("ex1data2.txt", delimiter=",", unpack=False)

X = data[:, 0:2]
y = data[:, 2:3]
#print(y)
m = len(y);

# Feature Normalization

(X, mu, sigma) = linear_regression.feature_normalize(X)

#Add intercept term to X
X = np.concatenate((np.ones((m,1)), X),1)


# Gradient Descent
print('Running gradient descent ...\n');

# Choose some alpha value
alpha = 0.01;
num_iters = 400;

# Initialize Theta and Run Gradient Descent 
theta = np.zeros((3, 1));
[theta, J_history] = linear_regression.gradient_descent(X, y, theta, alpha, num_iters)
print(theta)
theta = linear_regression.normal_eqn(X, y)
print(theta)

area = 1650 # sq-ft
area_norm = (area - mu[0]) / sigma [0]
no_of_bedrooms = 3
no_of_bedrooms_norm = (no_of_bedrooms - mu[1]) / sigma[1]
price = np.array([[1, area_norm, no_of_bedrooms_norm]]).dot(theta)
print(price)

