# -*- coding: utf-8 -*-
"""
Created on Sat Dec 24 21:27:09 2016

@author: arun
"""
import numpy as np

def feature_normalize(X): 
    
# FEATURENORMALIZE Normalizes the features in X 
#   FEATURENORMALIZE(X) returns a normalized version of X where
#   the mean value of each feature is 0 and the standard deviation
#   is 1. This is often a good preprocessing step to do when
#   working with learning algorithms.
#   X is a matrix where each column is a feature and each row is a training example.
    
    
    
    no_of_features = np.size(X, 1) # number of columns
    no_of_training_examples = np.size(X, 0) # number of columns
#    mu and standard deviation for all features
    X_norm = X # initialize
    mu = np.mean(X,0) # calculate mean
    sigma = np.std(X,0) # calculate sigma
   

    for i in range(0,no_of_features): # columns
        for j in range(0,no_of_training_examples): # rows
#    for i = 1:noOfFeatures % for each feature
#        % calculate the norm for all rows in each feature
#        X_norm(:,i) = (X(:,i) - mu(i))/sigma(i);           
           X_norm[j,i] = (X[j,i] - mu[i])/sigma[i]
#    end
#    
    return (X_norm, mu, sigma)
    

def compute_cost(X, y, theta):
    # COMPUTECOSTMULTI Compute cost for linear regression with multiple variables
    #   J = COMPUTECOSTMULTI(X, y, theta) computes the cost of using theta as the
    #   parameter for linear regression to fit the data points in X and y
    
    # Initialize some useful values
    m = len(y) # number of training examples

    #% X is already normalized and the intercept terms (ones) are already added
   
    return (1/(2*m))*(X.dot(theta)-y).transpose().dot(X.dot(theta)-y)

def gradient_descent(X, y, theta, alpha, num_iters):
    #GRADIENTDESCENTMULTI Performs gradient descent to learn theta
    #   theta = GRADIENTDESCENTMULTI(x, y, theta, alpha, num_iters) updates theta by
    #   taking num_iters gradient steps with learning rate alpha
    
    # Initialize some useful values
    m = len(y) # number of training examples
    J_history = np.zeros((num_iters, 1));    
    no_of_features = np.size(X, 1)-1 # number of features
    
    for iter in range (0,num_iters):        
        for i in range(0,no_of_features+1):
            # Update theta values
            theta[i,0] = theta[i,0]-(alpha/m) * (X.dot(theta)-y).transpose().dot(X[:,i])
    J_history[iter] = compute_cost(X, y, theta);
        
    return (theta,J_history)
    
def normal_eqn(X, y):
#NORMALEQN Computes the closed-form solution to linear regression 
#   NORMALEQN(X,y) computes the closed-form solution to linear 
#   regression using the normal equations.

    a1 = X.transpose().dot(y)
    a2 = X.transpose().dot(X)
    a3 = np.linalg.pinv(a2)
    a4 = a3.dot(a1)    
    return a4
#    return np.linalg.pinv((X.transpose().dot(X)).dot(X.transpose().dot(y)));




