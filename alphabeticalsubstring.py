"""
Write a program that prints the longest substring of s in which the letters occur in alphabetical order. For example, if s = 'azcbobobegghakl', then your program should print

Longest substring in alphabetical order is: beggh

In the case of ties, print the first substring. For example, if s = 'abcbcd', then your program should print

Longest substring in alphabetical order is: abc
"""

s = 'azcbobobegghakl'

new_answer = s[0]
index = 0
final_answer = ""

while index < (len(s) - 1):
    if s[index+1] >= s[index]:
        new_answer = new_answer + s[index+1]
    else:
        if len(new_answer)>len(final_answer):
            final_answer = new_answer
        new_answer = s[index+1]
    index = index + 1
if len(new_answer)>len(final_answer):
    final_answer = new_answer
if final_answer == "":
    final_answer = s
print("Longest substring in alphabetical order is:" + final_answer)
