def isIn(char, aStr):
    '''
    char: a single character
    aStr: an alphabetized string
    
    returns: True if char is in aStr; False otherwise
    '''
    # Your code here
    if aStr == '':
        return False
    if (char == aStr[(len(aStr)//2)]):
        return True
    elif char < aStr[(len(aStr)//2)]:
        return isIn(char, aStr[:(len(aStr)//2)])
    else:
        return isIn(char, aStr[(len(aStr)//2)+1:])    


print (isIn('u', 'jmo'))

# Arun Narayanan
# Written on June 23, 2014
# To count the number of times a string s1 occurs in another string s2

def in_string (s1, s2):
	"""
	(string, string) --> int
	
	returns the number of times a string s1 occurs in another string s2.

	Example:
	>>> in_string("s", "stringsonsantoors")
	4
	>>> in_string("run", "rtrertgerte")
	0
	>>> in_string("run", "runadasfrarunaafarjnkluinhhu")
	2
	>>> in_string("", "blankcharacter")
	15
	
	"""
	# Initializations 
	count = 0
		
	for i in range(len(s2)+1):
		if s2[i:i+len(s1)] == s1:
			count +=1
	return count

print(in_string("", "blankcharacter"))

# IMPROVEMENTS

# The following example in docstring

# >>> in_string("", "blankcharacter")
#	15

# must be rectified
