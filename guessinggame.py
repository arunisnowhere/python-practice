# In this problem, you'll create a program that guesses a secret number!

# The program works as follows: you (the user) thinks of an integer between 0 (inclusive) and 100 (not inclusive). The computer makes guesses, and you give it input - is its guess too high or too low? Using bisection search, the computer will guess the user's secret number!

# Your program should use bisection search. So think carefully what that means. What will the first guess always be? How should you calculate subsequent guesses? 

# Your initial endpoints should be 0 and 100. Do not optimize your subsequent endpoints by making them be the halfway point plus or minus 1. Rather, just make them be the halfway point. 



guessed = False
high = 100
low = 0
count = 0
print("Please think of an integer between 0 and 100!")
while not guessed:    
    count+=1
    secret_number=int((high+low)/2)
    print("Is your secret number " + str(secret_number) + "?")
    accuracy = input("Enter 'h' to indicate the guess is too high. Enter 'l' to indicate the guess is too low. Enter 'y' to indicate I guessed correctly. ")
    if accuracy == "h":
        high = secret_number
    elif accuracy == "l":
        low = secret_number
    elif accuracy == "y":       
        guessed = True
    else:
        print("Sorry, I did not understand your input.")
print("Game over. Your secret number was: ", secret_number, 'and I took just', count, 'attempts to find it!')

