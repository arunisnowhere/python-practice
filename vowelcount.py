# Arun Narayanan
# Written on June 23, 2014
# To count the number of vowels contained in a string

def vowelcount(s):
    """
    (str) --> int
    returns the number of vowels contained in a string s
    Example:
    >>> vowelcount("string")
    1
    >>> vowelcount("rotatetheworld")
    5
    >>> vowelcount("rhythm")
    0
    """
    count = 0
    for char in s:        
        if char in "aeiou" or char in "AEIOU":
            count +=1
    return count

print(vowelcount("rotatetheworld"))

