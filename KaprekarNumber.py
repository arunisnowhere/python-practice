# -*- coding: utf-8 -*-
"""
Created on Sat Dec  3 11:00:01 2016

@author: arun
"""
import math
import matharun

import os
cwd = os.getcwd()


def largest_digit(number):
    largest = 0
    while number > 0:
        if number % 10 >  largest:
             largest = number % 10
        number = number // 10
    return largest

# print (largest_digit(12345)) # 4
# print (largest_digit(3253)) # 5
# print (largest_digit(9800)) # 9
# print (largest_digit(3333)) # 3
# print (largest_digit(120))  # 2

def list_to_number(list):
    number = 0
    for i in range(0,len(list)):        
        number = number + list[i] * 10**(len(list)-i-1)        
    return number
    
def number_to_list(number):
    return [int(i) for i in str(number)]
    
def string_to_list(string):
    return [int(char) for char in string]

# print(number_to_list(12234))

def desc_digits(number):    
    # print (number_list(number))
    number_as_string = str(number)
    if len(number_as_string) == 3:
        number_as_string = '0'+ number_as_string
    if len(number_as_string) == 2:
        number_as_string = '00'+ number_as_string
    if len(number_as_string) == 1:
        number_as_string = '000'+ number_as_string    
    new_list = string_to_list(number_as_string)
    new_list.sort()
    new_list.reverse()    
    return list_to_number(new_list)   
    
def asc_digits(number):    
    # print (number_list(number))
    number_as_string = str(number)
    if len(number_as_string) == 3:
        number_as_string = '0'+ number_as_string
    if len(number_as_string) == 2:
        number_as_string = '00'+ number_as_string
    if len(number_as_string) == 1:
        number_as_string = '000'+ number_as_string
    new_list = string_to_list(number_as_string)
    new_list.sort()    
    return list_to_number(new_list)   
    
# print (desc_digits(1234)) # 4321
# print (desc_digits(3253)) # 5323
# print (desc_digits(9800)) # 
# print (desc_digits(3333)) # 3
# print (desc_digits(120))  # 2

def Kaprekar_routine(number):
    
    iteration_count = 0
    while number != 6174 and number != 0:
        iteration_count += 1
        print(number)
        print(desc_digits(number), '-', asc_digits(number), '=', desc_digits(number) - asc_digits(number))        
        number = desc_digits(number) - asc_digits(number)        
    return iteration_count

# print (Kaprekar_routine(6589))
# print (Kaprekar_routine(3345))
#print (Kaprekar_routine(5432))
#print (Kaprekar_routine(3333))
#print (Kaprekar_routine(14))

'''
max_iteration = 1
for number in range(1,9999):

    # print (number, ':', Kaprekar_routine(number))
    if max_iteration < Kaprekar_routine(number):
        max_iteration = Kaprekar_routine(number)
        max_iteration_number = number

print(max_iteration, max_iteration_number)
''' 
def Kaprekar_number(start_number,end_number):    
    K_number = []
    for number in range(start_number, end_number+1):        
        number_square = number**2
        count = 0
        number_square1 = 0
        while number_square1 != number_square:
            count += 1
            number_square1 = number_square % (10 ** count)
            number_square2 = number_square // (10 ** count)            
            if number_square1 + number_square2 == number:
                K_number.append(number)  
    return K_number
        
    
    
print (Kaprekar_number(101, 9000))