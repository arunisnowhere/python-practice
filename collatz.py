def collatz(number):
    """
	Collatz Conjecture
	Given any initial natural number, consider the sequence of numbers generated by repeatedly following the rule:
	   divide by two if the number is even or
	  multiply by 3 and add 1 if the number is odd.
	"""
    try:
        while (number > 1):
            print(number)
            
            if number % 2 == 0:
                number = number/2           
            else:
                number = number * 3 + 1
        return number
    except TypeError:
        print('Only integers allowed')    
    
    
print(collatz(217.5))
print(collatz('217'))
		

# # Raise exception for non-integers?

